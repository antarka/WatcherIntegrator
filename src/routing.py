#  --- Imports ---
from src.domain import *
from bottle import Bottle, request, run
app = Bottle()

# --- GO ---
@app.post('/tag/service/<name>.json')
def post_push_tag_service(name):
    ThreadPostTagService(name, json.loads(request.body.getvalue().decode('utf-8'))).start()
    return "Spam pas ok ?"

@app.post('/push/service/<name>.json')
def post_push_service(name):
    ThreadPostPushService(name, json.loads(request.body.getvalue().decode('utf-8'))).start()
    return "Spam pas ok ?"
