from config.config import config
import json, subprocess, os, logging, logging.config, smtplib
from email.mime.text import MIMEText
from string import Template
from datetime import datetime
from src.my_log import logger
from threading import Thread

# --- Globals ---
ROOT_FOLDER = os.path.dirname(os.path.dirname(__file__))
LOG = None
LOG_SHELL = """%(file)s
code de retour : %(return_code)d
sortie standard :
%(stdout)s
sortie d'erreur :
%(stderr)s
"""

# --- Functions ---
def go_root():
    logger.info('cd %s' % ROOT_FOLDER)
    os.chdir(ROOT_FOLDER)


go_root()

def sendmail(expediteur, destinataire, sujet, message, cc=None):
    """
    Permet d'envoyer un mail en smtp en ce basant sur la variabble globale config['smtp]
    contenant host port login password

    :param expediteur: str
    :param destinataire: str
    :param sujet: str
    :param message: Message
    :param cc : str | list
    """
    logger.info('>>> %s.sendmail', __file__)

    smtp = config['smtp']
    destinataires = [destinataire]
    message = MIMEText(message.content, message.type)
    message['Subject'] = sujet
    message['From'] = expediteur
    message['To'] = destinataire
    if cc is not None:
        if isinstance(cc, list):
            message['Cc'] = '%s\r\n' % ','.join(cc)
            destinataires += cc
        elif isinstance(cc, str):
            message['Cc'] = cc
            destinataires += [cc]
    server = smtplib.SMTP(smtp['host'], smtp['port'])
    server.starttls()
    server.login(smtp['login'], smtp['password'])
    server.sendmail(expediteur, destinataires, message.as_string())
    server.close()

    logger.debug(message.as_string())
    logger.info('<<< %s.sendmail', __file__)


# --- Class ---
class Message():
    def __init__(self, content, type):
        self.content = content
        self.type = type

with open('tpl/log.html', 'r') as file:
    LOG = file.read()

class ThreadPostTagService(Thread):
    def __init__(self, name, request):
        Thread.__init__(self)
        self.name = name
        self.request = request

    def run(self):
        """
            On verifie que le name existe dans la config
            On importe en local le depot
            execution du test_import.sh
                execution du maj.sh si le test c'est bien passé (dans l'environnement de production)
            Envoie par mail des logs
        """
        name = self.name
        request = self.request
        logger.info('>>> %s.post_tag_service(%s)', __file__, name)

        ag = Template(LOG)
        replacer = {
            'service_name': name,
            'event_type': 'tag push',
            'date': datetime.today().isoformat(),
            'display_test': 'block',
            'display_maj': 'block',
            'display_no_log': 'none'
        }

        services = config['services']
        if name in services and 'active' in services[name] and services[name]['active']:
            """ Recuperation et traitement des infos """
            logger.info('le service %s est présent et actif', name)

            service = services[name]
            req = request
            logger.debug('Contenue de la requete : %r', req)
            *_, tag = req['ref'].split('/')
            git_url = service['git_ssh_url']

            """ Lancement de test_import.sh """
            bash = subprocess.Popen(
                    args=['/bin/bash', '%s/test_import.sh' % os.getcwd(), tag, name, git_url],
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE
            )
            bash_return = bash.wait()
            bash_stdout = bash.stdout.read().decode("utf-8")
            bash_stderr = bash.stderr.read().decode("utf-8")
            bash.stdout.read().decode("utf-8")
            logger.warning(LOG_SHELL, {'return_code': bash_return, 'stdout': bash_stdout,
                           'stderr': bash_stderr, 'file': 'test_import.sh'})
            if bash_return == 0:
                """
                    Cool le test c'est bien passé, on va puvoir deployer le service
                    lancer le script maj.sh du depot
                """

                replacer['error_test'] = "Code de retour 0, R.A.S."

                maj = subprocess.Popen(
                        args=['/bin/bash', '%s/maj.sh' % service['root_folder'], tag],
                        stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE
                )
                maj_return = maj.wait()
                maj_stdout = maj.stdout.read().decode("utf-8")
                maj_stderr = maj.stderr.read().decode("utf-8")
                logger.warning(LOG_SHELL, {'return_code': maj_return, 'stdout': maj_stdout,
                               'stderr': maj_stderr, 'file': 'maj.sh'})
                if maj_return == 0:
                    replacer['error_maj'] = "Code de retour 0, R.A.S."
                elif maj_return == 1:
                    replacer['error_maj'] = "Error 1, Impossible d'installer les paquets natifs"
                elif maj_return == 2:
                    replacer['error_maj'] = "Error 2, Impossible d'installer les dependances"
                elif maj_return == 3:
                    replacer['error_maj'] = "Error 3, Erreur pendant la compilation des sources"
                elif maj_return == 4:
                    replacer['error_maj'] = "Error 4, Impossible de lancer le programme :"
                else:
                    replacer['error_maj'] = "Error %d, Erreur inconnue" % maj_return

                replacer['error_maj_class'] = 'ok' if maj_return == 0 else 'error'
                replacer['maj_stdout'] = maj_stdout
                replacer['maj_stderr'] = maj_stderr

            elif bash_return == 1:
                replacer['error_test'] = "Error 1, Impossible d'installer les paquets natifs"
            elif bash_return == 2:
                replacer['error_test'] = "Error 2, Impossible d'installer les dependances"
            elif bash_return == 3:
                replacer['error_test'] = "Error 3, Erreur pendant la compilation des sources"
            elif bash_return == 4:
                replacer['error_test'] = "Error 4, Impossible de lancer le programme :"
            else:
                replacer['error_test'] = "Error %d, Erreur inconnue" % bash_return

            replacer['error_test_class'] = 'ok' if bash_return == 0 else 'error'
            replacer['test_stdout'] = bash_stdout
            replacer['test_stderr'] = bash_stderr

        else:
            replacer['display_no_log'] = 'block'
            replacer['display_test'] = 'none'
            replacer['display_maj'] = 'none'

        msg = Message(ag.safe_substitute(replacer), 'html')
        sendmail(
                config['smtp']['login'],
                config['smtp']['login'],
                'Push Tag Event : {name} - {date}'.format(name=name, date=replacer['date']),
                msg,
                cc=service['mails'] if 'mails' in service else None
        )
        logger.info('<<< %s.post_tag_service(%s)', __file__, name)


class ThreadPostPushService(Thread):
    def __init__(self, name, request):
        Thread.__init__(self)
        self.name = name
        self.request = request

    def run(self):
        """
            Deroulement des actions lors d'un push event
            Il sera utile pour des projets qui n'ont pas besoins d'etre lancé
            un site static, ou un site php par exemple

            Pas besoins d'une phase de test
            pas besoins de preciser l'url du depot git vu qu'on ce contente d'un pull sur le serveur existant

            0. on verifie que le push a bien lieu sur la branche master
            1. on ce positionne sur le dossier du projet (config['services'][name]['root_folder'])
            2. on lance le fichier maj.sh
                1. git submodule update --init; git pull --recurse-submodules
                2. eventuellement des actions de build
                    - eventuellement si on utilise des preprocesseurs css ou js par exemple
            3. Envoi par mail du log
        """
        name = self.name
        request = self.request
        
        logger.info('>>> %s.post_push_service(%s)', __file__, name)
        ag = Template(LOG)
        replacer = {
            'service_name': name,
            'event_type': 'push',
            'date': datetime.today().isoformat(),
            'display_test': 'none',
            'display_maj': 'block',
            'display_no_log': 'none'
        }
        services = config['services']

        if name in services and 'active' in services[name] and services[name]['active']:
            logger.info('le service %s est présent et actif', name)
            service = services[name]
            req = request

            if req['ref'] == 'refs/heads/master':
                """ On est bien sur la branche master """
                maj = subprocess.Popen(
                        args=['/bin/bash', '%s/maj.sh' % service['root_folder']],
                        stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE
                )
                maj_return = maj.wait()
                maj_stdout = maj.stdout.read().decode("utf-8")
                maj_stderr = maj.stderr.read().decode("utf-8")
                logger.warning(LOG_SHELL, {'return_code': maj_return, 'stdout':maj_stdout,
                               'stderr':maj_stderr, 'file':'maj.sh'})

                replacer['error_maj_class'] = 'ok' if maj_return == 0 else 'error'
                replacer['error_maj'] = 'Error %d' % maj_return
                replacer['maj_stdout'] = maj_stdout
                replacer['maj_stderr'] = maj_stderr
            else:
                replacer['display_no_log'] = 'block'
                replacer['display_maj'] = 'none'

            replacer['display_test'] = 'none'

        msg = Message(ag.safe_substitute(replacer), 'html')
        smtp = config['smtp']
        sendmail(
                smtp['login'],
                smtp['login'],
                'Push Event : {name} - {date}'.format(name=name, date=replacer['date']),
                msg,
                cc=service['mails'] if 'mails' in service else None
        )
        logger.info('<<< %s.post_push_service(%s)', __file__, name)