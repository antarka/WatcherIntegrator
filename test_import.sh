#!/bin/bash

# Arguments donné par le watcher
tag=$1
name=$2
git_url=$3

echo "tag : ${tag}"
echo "name : ${name}"
echo "git_url : ${git_url}"

# On ce déplace dans le répertoire du script
self_path=$(readlink -f "$0")
self_directory=$(dirname "${self_path}")
cd "${self_directory}"

set -x
mkdir ${name}

# récupération du dépot
git clone ${git_url} ${name}/${tag} --recursive
cd ${name}/${tag}
git checkout ${tag}

# Installation des dépendances et lancement des tests si test.sh présent
if [ -f test.sh ]; then
    bash test.sh
fi

# Nettoyage du dossier de test
cd ${self_directory}
command rm -rf ${name}/${tag}

exit ${?}
