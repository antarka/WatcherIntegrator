#!/bin/bash

self_path=$(readlink -f "$0")
self_directory=$(dirname "${self_path}")
cd "${self_directory}"

# update repository
git submodule update --init
git pull --recurse-submodules
