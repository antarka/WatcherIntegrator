#!/bin/bash

self_path=$(readlink -f "$0")
self_directory=$(dirname "${self_path}")
cd "${self_directory}"
tag=$1

iquit() {
	local error=$1
	local exitcode=$2

	if [ "$error" -ne "0" ]; then
		exit $exitcode
	fi
}

# stop daemon
bash daemon.sh stop; iquit $? 4

# update repository
git submodule update --init
git pull --recurse-submodules

# install new dependency
npm install; iquit $? 2
# build new version
npm run build; iquit $? 3

#
# Attente du fichier de config .gitignorer
# pour pouvoir l'éditer tranquilement
# Et tester le lancement
#

# restart daemon
bash daemon.sh start; iquit $? 4
exit 0

