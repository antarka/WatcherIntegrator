config = {
    # pour configurer le port à utiliser pour le watcher
    'watcher': {
        'port': 8080,
        'host': 'localhost'
    },

    # Tableau associatif de services
    'services': {
        # Exemple de dépôt pour les tag push event
        # Ethernium est la clé : le <name> dans l'url du webhook
        'Ethernium': {
            # mettre sur False pour désactiver le service
            'active': True,
            # Définition du chemin absolu du dossier de prod
            'root_folder': '/path/to/ethernium',
            # URL du dépôt distant (obligatoire pour les tag push event)
            'git_ssh_url': 'git@github.com:superpowers/superpowers.git',
            # list d'adresse mail, ils recevront par mail les logs
            'mails': [
                'purexo@antarka.com',
                'fraxken@antarka.com'
            ]
        },
        # Exemple de dépôt pour les push event
        'Ethernium-Conception': {
            'active': True,
            'root_folder': '/path/to/conception',
            # mails peut être une simple chaine de caractère, pas obligé que ce soit une liste
            'mails': 'purexo@antarka.com'
        }
    },

    # Pour configurer avec quel serveur SMTP vous souhaitez envoyer vos mail de notif
    # Pour le moment, obligé d'avoir un service en startssl
    'smtp': {
        'host': 'gogol.com',
        'port': 1337,
        'login': 'bjbjio@gogol.com',
        'password': 'password'
    }
}