__author__ = 'Purexo'
from src import routing
run = routing.run
config = routing.config

if __name__ == '__main__':
    run(routing.app,
        host='localhost',
        port=config['watcher']["port"] or 8080,
        reloader=True
    )
    pass